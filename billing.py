#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    def on_change_payment_amount(self, vals):
        company_obj = Pool().get('company.company')
        journal_obj = Pool().get('account.journal')

        res = super(BillingLine, self).on_change_payment_amount(vals)

        args = [('type', '=', 'cash')]
        journal_ids = journal_obj.search(args)
        if len(journal_ids)==1:
            res['payment_journal'] = journal_ids[0]

        return res

BillingLine()
