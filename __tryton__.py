#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Billing Cash Journal',
    'name_de_DE': 'Fakturierung Rechnungsstellung aus Abrechnungsdaten Barzahlung Journal',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Account Invoice Billing Cash Journal
    - Sets the cash journal on billing lines
''',
    'description_de_DE': '''Zahlungsjournal für Fakturierung Rechnungsstellung
            aus Abrechnungsdaten mit Barzahlung
    - Setzt das Zahlungsjournal für Abrechnungszeilen
''',
    'depends' : [
        'account_invoice_billing_cash'
    ],
    'xml' : [
    ],
    'translation': [
    ],
}
